#!/usr/bin/python
#coding=utf-8

import Tkinter
import tkFileDialog
from socket import *
import struct
import os

def client(filename):
    
    Addr=('localhost',8000)
    BUFSIZE=1024

    Sendsocket=socket(AF_INET,SOCK_STREAM)
    Sendsocket.connect(Addr)

    FileHead=struct.pack('128s11i',filename,0,0,0,0,0,0,0,0,os.stat(filename).st_size,0,0)

    Sendsocket.send(FileHead)

    fp=open(filename,'rb')

    while 1:
        FileData=fp.read(BUFSIZE)
        if not FileData: break
        Sendsocket.send(FileData)

    fp.close()
    Sendsocket.close()

def FileOpen():
    r = tkFileDialog.askopenfilename(title = 'Python Tkinter',      
        filetypes=[('All files', '*')] )
    filename=r.split('/')
    client(filename[-1])
                                        
root = Tkinter.Tk()
button1 = Tkinter.Button(root, text = 'File Open', height=8,width=14,bg='blue',            
                         command = FileOpen)
button1.pack()
root.mainloop()


